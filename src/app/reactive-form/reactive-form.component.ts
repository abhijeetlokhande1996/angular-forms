import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css'],
})
export class ReactiveFormComponent implements OnInit, OnDestroy {
  regForm: FormGroup;
  subArr: Array<Subscription> = [];
  constructor() {}

  ngOnInit(): void {
    this.regForm = new FormGroup({
      givenName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(4),
      ]),
      emailId: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required, this.checkAgeRange]),
    });
    let sub = this.regForm.valueChanges.subscribe((values) => {
      console.log(values);
    });
    this.subArr.push(sub);
    sub = this.regForm.statusChanges.subscribe((status) => {
      console.log(status);
    });
    this.subArr.push(sub);
  }
  checkAgeRange(control: AbstractControl) {
    if (control.value) {
      const val = control.value;
      if (val > 0 && val < 101) {
        return null;
      }
    }
    return { age: true };
  }
  onSubmit() {
    console.log(this.regForm.value);
    //this.regForm.get('givenName').setValue(null);
    this.regForm.reset();
  }
  ngOnDestroy(): void {
    for (const sub of this.subArr) {
      sub.unsubscribe();
    }
  }
}
