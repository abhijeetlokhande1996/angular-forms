import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  @ViewChild('regForm') regForm;
  givenName: string;
  emailId: string;
  password: string;
  constructor() {}
  ngOnInit(): void {}

  submitFunc() {
    console.log(this.regForm);
  }
}
